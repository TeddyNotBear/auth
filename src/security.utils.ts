import * as crypto from 'crypto';
import {TransformFnParams} from "class-transformer";

export class SecurityUtils{

    static sha512(str: string): string{
        return crypto.createHash('sha512').update(str).digest('hex');
    }

    static generateToken(length: number): string {
        return crypto.randomBytes(length / 2).toString('hex');
    }
}

export function PasswordTransformHandler(params: TransformFnParams): any{
    if(typeof params.value === 'string' && params.value.length >= 6){
        return SecurityUtils.sha512(params.value);
    }
    return params.value;
}