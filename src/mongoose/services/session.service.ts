import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {SessionSchema, ISessionDocument} from "../schemas";
import {AccountCreateDTO, ISession} from "../../definitions";
import {SecurityUtils} from "../../security.utils";
import {SessionCreateDTO} from "../../definitions/dto/session/session-create.dto";

export class SessionService extends BaseService{
    protected model: Model<ISessionDocument>

    constructor(connection: Mongoose){
        super(connection);
        this.model = connection.model<ISessionDocument>('Session',SessionSchema);
    }

    public create(dto: SessionCreateDTO):Promise<ISessionDocument>{
        return this.model.create(dto)
    }

    public getByToken(token: string): Promise<ISessionDocument | null> {
        return this.model.findOne({
            token
        }).populate('account').exec();
    }
}