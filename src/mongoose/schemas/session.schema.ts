import {Schema, SchemaTypes} from "mongoose";
import {IAccount, ISession} from "../../definitions";

export const SessionSchema = new Schema({
    token: {
        type: SchemaTypes.String,
        required: true,
        length: 32,
        unique: true
    },
    platform: {
        type: SchemaTypes.String,
        required: true,
    },
    account: {
        type: SchemaTypes.ObjectId,
        ref: "Account",
    },
}, {
    timestamps: {
        createdAt: 'createdDate',
        updatedAt: 'updatedDate'
    },
    versionKey: false,
    collection: 'sessions'
});

export type ISessionDocument = ISession & Document;