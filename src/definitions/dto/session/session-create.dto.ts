import {IsDate, IsMongoId, IsOptional, IsString, Length, MaxDate, MaxLength, Min, MinLength} from "class-validator";
import {Expose, Transform, Type} from "class-transformer";
import {PasswordTransformHandler} from "../../../security.utils";

export class SessionCreateDTO {

    @MaxLength(32)
    @MinLength(32)
    @IsString()
    @Expose()
    token: string;

    @MinLength(1)
    @IsString()
    @Expose()
    platform: string;

    @IsMongoId()
    @Expose()
    account: string;
}