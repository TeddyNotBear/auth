import {IsDate, IsOptional, IsString, MaxDate, MinLength} from "class-validator";
import {Expose, Transform, Type} from "class-transformer";
import {PasswordTransformHandler} from "../../../security.utils";

export class AccountCreateDTO {

    @MinLength(4)
    @IsString()
    @Expose()
    login: string;

    @MinLength(6)
    @IsString()
    @Expose()
    @Transform(PasswordTransformHandler)
    password: string;

    @MinLength(1)
    @IsString()
    @Expose()
    firstName: string;

    @MinLength(1)
    @IsString()
    @Expose()
    lastName: string;

    @IsDate()
    @MaxDate(new Date())
    @IsOptional()
    @Expose()
    @Type(() => Date)
    birthDate?: Date;
}