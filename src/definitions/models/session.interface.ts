import {IAccount} from "./account.interface";

export interface ISession{
    _id: any; // ObjectID
    token: string;
    platform: string;
    account : string | IAccount
}