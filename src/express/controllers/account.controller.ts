import {BaseController} from "./base.controller";
import {Router, Request, Response} from "express";
import {MongooseUtils} from "../../mongoose";
import {AccountCreateDTO, AccountLoginDTO} from "../../definitions";
import {plainToInstance} from "class-transformer";
import {SessionCreateDTO} from "../../definitions/dto/session/session-create.dto";
import {SecurityUtils} from "../../security.utils";
import {sessionMiddleware} from "../middleware";

export class AccountController extends BaseController{

    async subscribe(req: Request, res: Response){
        const dto = await AccountController.createAndValidateDTO(AccountCreateDTO, req.body,res);
        if(!dto) return;

        try{
            const account = await this.serviceRegistry.accountService.create(dto);
            res.status(201).json(account);
        }catch(err){
            if(MongooseUtils.isDuplicateKeyError(err)){
                res.status(409).end();
            }

            res.status(500).end();
        }
    }

    async login(req: Request, res: Response){
        const dto = await AccountController.createAndValidateDTO(AccountLoginDTO, req.body,res);
        if(!dto) return;

        const account = await this.serviceRegistry.accountService.getWithLogin(dto);
        if(!account) {
            res.status(402).end();
            return;
        }

        const sessionDTO = await AccountController.createAndValidateDTO(SessionCreateDTO, {
            token: SecurityUtils.generateToken(32),
            platform: req.header('user-agent'),
            account: account._id.toString()
        }, res);
        if(!sessionDTO) {
            return;
        }

        const session = await this.serviceRegistry.sessionService.create(sessionDTO);

        res.status(201).json(session);
    }

    async me(req: Request, res: Response) {
        res.json(req.account);
    }

    buildRoutes(): Router {
        const router = Router();
        router.post('/subscribe',this.subscribe.bind(this));
        router.post('/login',this.login.bind(this));
        router.get('/me', sessionMiddleware(this.serviceRegistry), this.me.bind(this));
        return router;
    }
}