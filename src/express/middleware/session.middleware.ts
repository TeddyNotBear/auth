import {ServiceRegistry} from "../../mongoose";
import {NextFunction, Request, Response} from "express";
import {IAccount} from "../../definitions";

declare module 'express' {
    export interface Request {
        account?: IAccount;
    }
}

export function sessionMiddleware(registry: ServiceRegistry) {
    return async function (req: Request, res: Response, next: NextFunction) {
        const authorization = req.header('authorization');
        if(!authorization) {
            res.status(401).end()
            return;
        }
        const parts = authorization.split(' ');
        if(parts.length != 2 || parts[0] !== 'Bearer' || parts[1].length !== 32) {
            res.status(401).end();
            return;
        }
        const token = parts[1];
        const session = await registry.sessionService.getByToken(token);
        if(!session) {
            res.status(403).end() // forbidden access
            return;
        }
        req.account = session.account as IAccount;
        next(); // permet d'accéder à la route
    }
}